# CasualGame

**Implementation of Rock, Paper, Scissors casual game**

## Testing

1) Create uesrs
```bash
curl -v -H "Content-Type: application/json" -d '{"username": "oleg", "password": "123"}' -X POST http://localhost:4001/users

curl -v -H "Content-Type: application/json" -d '{"username": "tim", "password": "123"}' -X POST http://localhost:4001/users
```

2) Create challenge
```bash

curl -v -H "authorization: oleg:123" -X POST -H "Content-Type: application/json" localhost:4001/games/games-server
*   Trying 127.0.0.1...
* Connected to localhost (127.0.0.1) port 4001 (#0)
> POST /games/games-server HTTP/1.1
> Host: localhost:4001
> User-Agent: curl/7.43.0
> Accept: */*
> authorization: oleg:123
> Content-Type: application/json
>
< HTTP/1.1 200 OK
< server: Cowboy
< date: Mon, 15 Oct 2018 14:44:14 GMT
< content-length: 26
< cache-control: max-age=0, private, must-revalidate
< content-type: application/json; charset=utf-8
<
* Connection #0 to host localhost left intact
{"ok":"challenge created"}%

```

3) List challenges
```bash
curl -v -H "authorization: oleg:123" -H "Content-Type: application/json" localhost:4001/games/games-server
*   Trying 127.0.0.1...
* Connected to localhost (127.0.0.1) port 4001 (#0)
> GET /games/games-server HTTP/1.1
> Host: localhost:4001
> User-Agent: curl/7.43.0
> Accept: */*
> authorization: oleg:123
> Content-Type: application/json
>
< HTTP/1.1 200 OK
< server: Cowboy
< date: Mon, 15 Oct 2018 14:45:05 GMT
< content-length: 8
< cache-control: max-age=0, private, must-revalidate
< content-type: application/json; charset=utf-8
<
* Connection #0 to host localhost left intact
["oleg"]%
```

4) Accept challenge
```bash
curl -v -X POST -H "authorization: tim:123" -d '{"challenge": "oleg"}' -H "Content-Type: application/json" localhost:4001/games/games-server
*   Trying 127.0.0.1...
* Connected to localhost (127.0.0.1) port 4001 (#0)
> POST /games/games-server HTTP/1.1
> Host: localhost:4001
> User-Agent: curl/7.43.0
> Accept: */*
> authorization: tim:123
> Content-Type: application/json
> Content-Length: 21
>
* upload completely sent off: 21 out of 21 bytes
< HTTP/1.1 200 OK
< server: Cowboy
< date: Mon, 15 Oct 2018 14:46:40 GMT
< content-length: 45
< cache-control: max-age=0, private, must-revalidate
< content-type: application/json; charset=utf-8
<
* Connection #0 to host localhost left intact
{"ok":"208dc750-d089-11e8-9fb9-acbc32cad8e7"}%
```

5) Making moves
```bash
 curl -v -X POST -H "authorization: tim:123" -d '{"move": "rock"}' -H "Content-Type: application/json" localhost:4001/games/game/c8c05ffa-d089-11e8-855b-acbc32cad8e7
*   Trying 127.0.0.1...
* Connected to localhost (127.0.0.1) port 4001 (#0)
> POST /games/game/c8c05ffa-d089-11e8-855b-acbc32cad8e7 HTTP/1.1
> Host: localhost:4001
> User-Agent: curl/7.43.0
> Accept: */*
> authorization: tim:123
> Content-Type: application/json
> Content-Length: 16
>
* upload completely sent off: 16 out of 16 bytes
< HTTP/1.1 200 OK
< server: Cowboy
< date: Mon, 15 Oct 2018 14:51:53 GMT
< content-length: 4
< cache-control: max-age=0, private, must-revalidate
< content-type: application/json; charset=utf-8
<
* Connection #0 to host localhost left intact
"ok"%
```

