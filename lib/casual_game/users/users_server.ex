defmodule CasualGame.Users.UsersServer do
  @moduledoc """
  Process responsible for storing all information about users.
  """
  use GenServer

  alias CasualGame.Users.UsersServer
  defstruct [:storage]

  def start_link() do
    GenServer.start_link(__MODULE__, [], name: __MODULE__)
  end

  @spec create_user(binary(), binary()) :: boolean()
  def create_user(username, password) do
    GenServer.call(__MODULE__, {:create_user, username, password})
  end

  @spec get_user(binary(), binary()) :: {:ok, binary()} | {:error, :incorrect_credentials}
  def get_user(username, password) do
    GenServer.call(__MODULE__, {:get_user, username, password})
  end

  def update_leadboard(username, record_victory) do
    GenServer.cast(__MODULE__, {:update_leadboard, username, record_victory})
  end

  @spec user_info(binary()) :: [tuple()]
  def user_info(username) do
    GenServer.call(__MODULE__, {:user_info, username})
  end

  @spec users() :: list()
  def users() do
    GenServer.call(__MODULE__, :users)
  end

  @spec reset() :: :ok
  def reset() do
    GenServer.call(__MODULE__, :reset)
  end

  def init(_args) do
    tid = :ets.new(__MODULE__, [:set, :private])
    {:ok, %UsersServer{storage: tid}}
  end

  def handle_call({:get_user, username, password}, _from, state) do
    result =
      case :ets.lookup(state.storage, username) do
        [{^username, ^password, _, _}] -> {:ok, username}
        _ -> {:error, :incorrect_credentials}
      end

    {:reply, result, state}
  end

  def handle_call({:create_user, username, password}, _from, state) do
    result = :ets.insert_new(state.storage, {username, password, 0, 0})
    {:reply, result, state}
  end

  def handle_call(:users, _from, state) do
    {:reply, :ets.tab2list(state.storage), state}
  end

  def handle_call({:user_info, username}, _from, state) do
    {:reply, :ets.lookup(state.storage, username), state}
  end

  def handle_call(:reset, _from, state) do
    :ets.delete(state.storage)
    new_tid = :ets.new(__MODULE__, [:set, :private])
    {:reply, :ok, %UsersServer{state | storage: new_tid}}
  end

  def handle_cast({:update_leadboard, username, record_victory}, state) do
    case record_victory do
      true ->
        :ets.update_counter(state.storage, username, [{3, 1}, {4, 1}])

      false ->
        :ets.update_counter(state.storage, username, {3, 1})
    end

    {:noreply, state}
  end
end
