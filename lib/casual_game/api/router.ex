defmodule CasualGame.API.Router do
  use PlugRest.Router
  use Plug.ErrorHandler

  plug(Plug.Parsers, parsers: [:json], json_decoder: Poison)

  plug(:match)
  plug(:dispatch)

  resource("/users", CasualGame.API.UserResource)

  forward("/games", to: CasualGame.API.Games.Router)

  match "_" do
    send_resp(conn, 404, "Page not found")
  end
end
