defmodule CasualGame.API.UserResource do
  @moduledoc """
  This is the documentation for the UserResource module.

  See [PlugRest docs](https://hexdocs.pm/plug_rest/PlugRest.Resource.html)
  for more information on the REST callbacks.
  """

  use PlugRest.Resource

  alias CasualGame.Users.UsersServer

  def allowed_methods(conn, state) do
    {["HEAD", "GET", "POST"], conn, state}
  end

  # Checks if request is valid. Current function is quite incomplete, and
  # can't be extended. It's better to use https://github.com/jonasschmidt/ex_json_schema
  # for more complete data validation.
  def malformed_request(conn = %{method: "POST"}, state) do
    username = Map.get(conn.body_params, "username")
    password = Map.get(conn.body_params, "password")

    case {username, password} do
      {username, password} when is_nil(username) or is_nil(password) ->
        response = %{"errors" => "username and password must be provided"}
        conn = put_rest_body(conn, Poison.encode!(response))
        {true, conn, state}

      _ ->
        {false, conn, state}
    end
  end

  def malformed_request(conn, state), do: {false, conn, state}

  def content_types_provided(conn, state) do
    {[{"application/json", :to_json}], conn, state}
  end

  def content_types_accepted(conn, state) do
    {[{"application/json", :from_json}], conn, state}
  end

  def from_json(conn, state) do
    # Create actual user if possible
    username = Map.get(conn.body_params, "username")
    password = Map.get(conn.body_params, "password")
    result = UsersServer.create_user(username, password)

    case result do
      true ->
        conn = put_rest_body(conn, Poison.encode!(%{"ok" => "user_created"}))
        {true, conn, state}

      false ->
        resp_body =
          Poison.encode!(%{
            "error" => "user with this username is already created"
          })

        response = send_resp(conn, 409, resp_body)
        {:stop, response, state}
    end
  end

  # This function is provided only for testing purposes. It would be better to use
  # some sort of pagination to address possible performance issues.
  def to_json(conn, state) do
    body =
      UsersServer.users()
      |> Enum.map(fn {username, _password} -> username end)
      |> Poison.encode!()

    {body, conn, state}
  end
end
