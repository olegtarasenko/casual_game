defmodule CasualGame.API.AuthPlug do
  import Plug.Conn

  alias CasualGame.Users.UsersServer

  def init(options), do: options

  def call(conn, _options) do
    with {:ok, username, passord} <- extract_credentials(conn),
         {:ok, username} <- get_user(username, passord) do
      conn |> assign(:current_user, username)
    else
      _ ->
        conn
        |> put_status(:unauthorized)
        |> send_resp(401, "Unauthorized")
        |> halt()
    end
  end

  defp extract_credentials(conn) do
    auth_header = get_req_header(conn, "authorization")

    case auth_header do
      [] ->
        {:error, :incorrect_token}

      [username_password] ->
        do_extract_credentials(username_password)
    end
  end

  defp do_extract_credentials(username_password) do
    case String.split(username_password, ":") do
      [username, password] -> {:ok, username, password}
      _ -> {:error, :wrong_format_of_password}
    end
  end

  defp get_user(username, passord) do
    UsersServer.get_user(username, passord)
  end
end
