defmodule CasualGame.API.GamesServerResource do
  @moduledoc """
  This is the documentation for the UserResource module.

  See [PlugRest docs](https://hexdocs.pm/plug_rest/PlugRest.Resource.html)
  for more information on the REST callbacks.
  """
  alias CasualGame.Game.GamesServer

  use PlugRest.Resource

  def allowed_methods(conn, state) do
    {["HEAD", "GET", "POST"], conn, state}
  end

  def content_types_provided(conn, state) do
    {[{"application/json", :to_json}], conn, state}
  end

  def content_types_accepted(conn, state) do
    {[{"application/json", :from_json}], conn, state}
  end

  def from_json(conn, state) do
    case Map.get(conn.body_params, "challenge") do
      nil ->
        maybe_create_challenge(conn, state)

      challenge ->
        maybe_accept_challenge(conn, state, challenge)
    end
  end

  defp maybe_create_challenge(conn, state) do
    username = Map.get(conn.assigns, :current_user)

    result = GamesServer.create_challenge(username)

    case result do
      :ok ->
        conn = put_rest_body(conn, Poison.encode!(%{"ok" => "challenge created"}))
        {true, conn, state}

      {:error, :already_created} ->
        resp_body =
          Poison.encode!(%{
            "error" => "can't create more than one challenge"
          })

        response = send_resp(conn, 409, resp_body)
        {:stop, response, state}
    end
  end

  defp maybe_accept_challenge(conn, state, challenge) do
    username = Map.get(conn.assigns, :current_user)

    result = GamesServer.accept_challenge(username, challenge)

    case result do
      {:ok, game_id} ->
        conn = put_rest_body(conn, Poison.encode!(%{"ok" => game_id}))
        {true, conn, state}

      {:error, reason} ->
        resp_body = Poison.encode!(%{"error" => reason})
        response = send_resp(conn, 409, resp_body)
        {:stop, response, state}
    end
  end

  # This function is provided only for testing purposes. It would be better to use
  # some sort of pagination to address possible performance issues.
  def to_json(conn, state) do
    challenges =
      GamesServer.list_challenges()
      |> Poison.encode!()

    {challenges, conn, state}
  end
end
