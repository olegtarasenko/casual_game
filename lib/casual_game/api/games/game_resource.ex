defmodule CasualGame.API.GameResource do
  use PlugRest.Resource

  alias CasualGame.Game.GamesServer

  def allowed_methods(conn, state) do
    {["HEAD", "GET", "POST"], conn, state}
  end

  def content_types_provided(conn, state) do
    {[{"application/json", :to_json}], conn, state}
  end

  def content_types_accepted(conn, state) do
    {[{"application/json", :from_json}], conn, state}
  end

  def resource_exists(conn, _state) do
    game_id = Map.get(conn.params, "game_id")

    case GamesServer.game(game_id) do
      nil -> {false, conn, :no_game}
      _pid -> {true, conn, game_id}
    end
  end

  def from_json(conn, game_id) do
    username = Map.get(conn.assigns, :current_user)
    move = Map.get(conn.body_params, "move")

    result = GamesServer.user_move(game_id, username, move)
    conn = put_rest_body(conn, Poison.encode!(result))

    {true, conn, game_id}
  end

  def to_json(conn, game_id) do
    msg = GamesServer.game_status(game_id)
    {Poison.encode!(msg), conn, game_id}
  end
end
