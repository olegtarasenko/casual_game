defmodule CasualGame.API.Games.Router do
  use PlugRest.Router
  use Plug.ErrorHandler

  plug(CasualGame.API.AuthPlug)

  plug(Plug.Parsers, parsers: [:json], json_decoder: Poison)

  plug(:match)
  plug(:dispatch)

  # Putting following resources under auth proteciont

  resource("/games-server", CasualGame.API.GamesServerResource)
  resource("/game/:game_id", CasualGame.API.GameResource)
end
