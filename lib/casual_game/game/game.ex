defmodule CasualGame.Game.Game do
  @moduledoc """
  Game process
  """
  use GenServer
  require Logger

  alias CasualGame.Game.Game

  @moves ["rock", "paper", "scissors"]

  defstruct [:tref, :players, moves: %{}, round_winners: %{}, num_rounds: 2, subscribers: []]

  def start_link([username1, username2]) do
    GenServer.start(__MODULE__, [username1, username2])
  end

  @spec user_move(pid(), binary(), binary()) :: :ok | {:error, :already_moved}
  def user_move(pid, username, move) do
    GenServer.call(pid, {:user_move, username, move})
  end

  @spec game_status(pid()) :: map()
  def game_status(pid) do
    GenServer.call(pid, :game_status)
  end

  @spec subscribe(pid(), pid()) :: :ok
  def subscribe(pid, subscriber) do
    GenServer.call(pid, {:subscribe, subscriber})
  end

  def init([username1, username2]) do
    Logger.info(fn -> "Game between #{username1} and #{username2} started..." end)
    num_rounds = Application.get_env(:casual_game, :number_of_rounds, 10)
    move_timeout = Application.get_env(:casual_game, :move_timeout, 1000)

    # Sending a message to ourselves
    tref = Process.send_after(self(), :move_timeout, move_timeout)

    state = %Game{players: [username1, username2], num_rounds: num_rounds, tref: tref}
    {:ok, state}
  end

  def handle_call(:game_status, _from, state) do
    {:reply, state.round_winners, state}
  end

  def handle_call({:subscribe, subscriber}, _from, %Game{subscribers: subscribers} = state) do
    {:reply, :ok, %Game{state | subscribers: [subscriber | subscribers]}}
  end

  def handle_call({:user_move, username, move}, _from, state) do
    Logger.info(fn -> "#{username} uses #{move}" end)
    %Game{moves: moves} = state

    # Insert new move if not inserted yet.
    case Map.has_key?(moves, username) do
      false ->
        new_state = %Game{state | moves: Map.put(moves, username, move)}
        maybe_check_round_results(new_state)
        {:reply, :ok, new_state}

      true ->
        {:reply, {:error, :already_moved}, state}
    end
  end

  def handle_info(:check_round_results, state) do
    %Game{
      moves: moves,
      round_winners: round_winners,
      num_rounds: num_rounds,
      tref: tref,
      subscribers: subscribers
    } = state

    Process.cancel_timer(tref)

    new_round_winners =
      Map.update(round_winners, find_winner(moves), 1, fn
        value -> value + 1
      end)

    new_state = %Game{
      state
      | round_winners: new_round_winners,
        num_rounds: num_rounds - 1
    }

    send(self(), :maybe_start_next_round)
    notify_subscribers(subscribers, :round_finished)
    {:noreply, new_state}
  end

  # We don't have more rounds to play. Time to announce the winner
  def handle_info(:maybe_start_next_round, %Game{num_rounds: 0} = state) do
    Logger.info("Game have finished...")

    [player1, player2] = state.players
    player1_wins = Map.get(state.round_winners, player1)
    player2_wins = Map.get(state.round_winners, player2)

    cond do
      player1_wins == player2_wins ->
        CasualGame.Users.UsersServer.update_leadboard(player1, false)
        CasualGame.Users.UsersServer.update_leadboard(player2, false)

      player1_wins > player2_wins ->
        CasualGame.Users.UsersServer.update_leadboard(player1, true)
        CasualGame.Users.UsersServer.update_leadboard(player2, false)

      player2_wins > player1_wins ->
        CasualGame.Users.UsersServer.update_leadboard(player1, false)
        CasualGame.Users.UsersServer.update_leadboard(player2, true)
    end

    notify_subscribers(state.subscribers, :game_finished)
    {:stop, :normal, state}
  end

  # We still have another round to be done...
  def handle_info(:maybe_start_next_round, state) do
    Logger.info("Starting next round..")
    move_timeout = Application.get_env(:casual_game, :move_timeout, 1000)
    tref = Process.send_after(self(), :move_timeout, move_timeout)

    {:noreply, %Game{state | moves: %{}, tref: tref}}
  end

  def handle_info(:move_timeout, state) do
    Logger.info("Moves timeout. Generating missing moves now.")
    %Game{tref: tref, moves: moves, players: players} = state

    Process.cancel_timer(tref)
    new_moves = generate_missing_moves(players, moves)
    send(self(), :check_round_results)
    {:noreply, %Game{state | moves: new_moves}}
  end

  defp notify_subscribers(subscribers, message) do
    Enum.each(subscribers, fn subscriber -> send(subscriber, message) end)
  end

  defp generate_missing_moves(players, moves) do
    Enum.into(players, %{}, fn player ->
      case Map.has_key?(moves, player) do
        true ->
          # Just copy existing move (if it's done by player)
          {player, Map.get(moves, player)}

        false ->
          # Generate new move otherwise
          {player, Enum.random(@moves)}
      end
    end)
  end

  # Triggers checking of the round results
  defp maybe_check_round_results(%Game{moves: moves} = _state) do
    case map_size(moves) do
      2 ->
        send(self(), :check_round_results)
        :scheduled

      _ ->
        :ignore
    end
  end

  defp find_winner(moves) do
    [user1, user2] = Map.keys(moves)

    case compare(Map.get(moves, user1), Map.get(moves, user2)) do
      false ->
        Logger.info("#{user2} won")
        user2

      true ->
        Logger.info("#{user1} won")
        user1

      :draw ->
        Logger.info("Draw")
        :draw
    end
  end

  defp compare("rock", "paper"), do: false
  defp compare("rock", "scissors"), do: true
  defp compare("rock", "rock"), do: :draw
  defp compare("paper", "scissors"), do: false
  defp compare("paper", "rock"), do: true
  defp compare("paper", "paper"), do: :draw
  defp compare("scissors", "scissors"), do: :draw
  defp compare("scissors", "rock"), do: false
  defp compare("scissors", "paper"), do: true
end
