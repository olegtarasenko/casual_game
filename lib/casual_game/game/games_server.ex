defmodule CasualGame.Game.GamesServer do
  @moduledoc """
  Games manager
  """
  use GenServer

  alias CasualGame.Game.Game
  alias CasualGame.Game.GameSup
  alias CasualGame.Game.GamesServer

  require Logger

  @type game_id :: binary()

  defstruct challenges: [], games: %{}

  def start_link() do
    GenServer.start(__MODULE__, [], name: __MODULE__)
  end

  @spec game(game_id()) :: pid()
  def game(game_id) do
    GenServer.call(__MODULE__, {:game, game_id})
  end

  @spec games() :: list()
  def games(), do: GenServer.call(__MODULE__, :games)

  @spec create_challenge(binary()) :: :ok | {:error, :already_created}
  def create_challenge(username) do
    GenServer.call(__MODULE__, {:create_challenge, username})
  end

  @spec accept_challenge(binary(), binary()) ::
          {:ok, game_id()} | {:error, :cant_accept_own_challenge} | {:error, :challenge_expired}
  def accept_challenge(username, challenge) do
    GenServer.call(__MODULE__, {:accept_challenge, username, challenge})
  end

  @spec user_move(game_id(), binary(), binary()) :: :ok | {:error, :already_moved}
  def user_move(game_id, username, move) do
    GenServer.call(__MODULE__, {:user_move, game_id, username, move})
  end

  @spec game_status(game_id()) :: map()
  def game_status(game_id) do
    GenServer.call(__MODULE__, {:game_status, game_id})
  end

  @spec list_challenges() :: [binary()]
  def list_challenges() do
    GenServer.call(__MODULE__, :list_challenges)
  end

  @spec reset() :: :ok
  def reset() do
    GenServer.call(__MODULE__, :reset)
  end

  def init(_args) do
    {:ok, %GamesServer{}}
  end

  def handle_call(:reset, _from, _state) do
    {:reply, :ok, %GamesServer{}}
  end

  def handle_call(:games, _from, state) do
    {:reply, state.games, state}
  end

  def handle_call({:create_challenge, username}, _from, state) do
    %GamesServer{challenges: challenges} = state

    {reply, new_state} =
      case username in challenges do
        false ->
          new_challenges = [username | challenges]
          new_state = %GamesServer{state | challenges: new_challenges}

          {:ok, new_state}

        true ->
          {{:error, :already_created}, state}
      end

    {:reply, reply, new_state}
  end

  def handle_call({:accept_challenge, username, username}, _from, state) do
    {:reply, {:error, :cant_accept_own_challenge}, state}
  end

  def handle_call({:accept_challenge, username, challenge}, _from, state) do
    %GamesServer{challenges: challenges, games: games} = state

    {message, state} =
      case challenge in challenges do
        # Challenge is still active.
        true ->
          {:ok, pid} = DynamicSupervisor.start_child(GameSup, {Game, [username, challenge]})
          Process.monitor(pid)
          game_id = UUID.uuid1()
          new_games = Map.put(games, game_id, pid)
          new_challenges = challenges -- [challenge]
          {{:ok, game_id}, %{state | games: new_games, challenges: new_challenges}}

        false ->
          {{:error, :challenge_expired}, state}
      end

    {:reply, message, state}
  end

  def handle_call({:user_move, game_id, username, move}, _from, state) do
    game_pid = Map.get(state.games, game_id)
    result = Game.user_move(game_pid, username, move)
    {:reply, result, state}
  end

  def handle_call({:game_status, game_id}, _from, state) do
    game_pid = Map.get(state.games, game_id)

    result = Game.game_status(game_pid)
    {:reply, result, state}
  end

  def handle_call(:list_challenges, _from, state) do
    {:reply, state.challenges, state}
  end

  def handle_call({:game, game_id}, _from, state) do
    {:reply, Map.get(state.games, game_id), state}
  end

  def handle_info({:DOWN, _ref, :process, pid, _reason}, state) do
    new_games =
      case Enum.find(state.games, fn {_key, value} -> value == pid end) do
        nil ->
          state.games

        {key, _value} ->
          Map.delete(state.games, key)
      end

    {:noreply, %GamesServer{state | games: new_games}}
  end
end
