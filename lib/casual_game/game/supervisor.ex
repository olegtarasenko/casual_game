defmodule CasualGame.Game.Supervisor do
  use Supervisor

  def start_link() do
    Supervisor.start_link(__MODULE__, [], name: __MODULE__)
  end

  def init(_arg) do
    import Supervisor.Spec, warn: false

    children = [
      worker(CasualGame.Game.GamesServer, []),
      {DynamicSupervisor, name: CasualGame.Game.GameSup, strategy: :one_for_one}
    ]

    Supervisor.init(children, strategy: :one_for_one)
  end
end
