defmodule CasualGame.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    import Supervisor.Spec, warn: false

    # List all child processes to be supervised
    children = [
      supervisor(CasualGame.Game.Supervisor, []),
      worker(CasualGame.Users.UsersServer, []),
      Plug.Adapters.Cowboy.child_spec(:http, CasualGame.API.Router, [], port: 4001)
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: CasualGame.Supervisor]

    Supervisor.start_link(children, opts)
  end
end
