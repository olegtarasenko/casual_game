defmodule CasualGame do
  @moduledoc """
  Documentation for CasualGame.
  """

  @doc """
  Hello world.

  ## Examples

      iex> CasualGame.hello()
      :world

  """
  def hello do
    :world
  end
end
