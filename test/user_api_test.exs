defmodule UserApiTest do
  use ExUnit.Case
  use Plug.Test

  alias CasualGame.API.UserResource
  alias CasualGame.Users.UsersServer

  describe "users signup resource" do
    setup do
      on_exit(fn ->
        UsersServer.reset()
      end)
    end

    test "can create user resourse" do
      params = %{"username" => "test", "password" => "test"}

      conn =
        conn(:post, "/users")
        |> put_req_header("content-type", "application/json")
        |> Map.put(:body_params, params)
        |> UserResource.call([])

      assert conn.status == 200
      resp = conn.resp_body |> Poison.decode!()
      assert resp == %{"ok" => "user_created"}
    end

    test "error is returned when it's not possible to create user" do
      # Creating first user
      UsersServer.create_user("test", "123")

      # Making user with the same credentials
      params = %{"username" => "test", "password" => "test"}

      conn =
        conn(:post, "/users")
        |> put_req_header("content-type", "application/json")
        |> Map.put(:body_params, params)
        |> UserResource.call([])

      assert conn.status == 409
      resp = conn.resp_body |> Poison.decode!()
      assert resp == %{"error" => "user with this username is already created"}
    end

    test "requests are validated" do
      # Preparing incomplete creation params
      params = %{"password" => "test"}

      conn =
        conn(:post, "/users")
        |> put_req_header("content-type", "application/json")
        |> Map.put(:body_params, params)
        |> UserResource.call([])

      assert conn.status == 400
      resp = conn.resp_body |> Poison.decode!()
      assert resp == %{"errors" => "username and password must be provided"}
    end
  end
end
