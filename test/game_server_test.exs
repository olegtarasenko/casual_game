defmodule CasualGameTest.GameServerTests do
  use ExUnit.Case, async: false

  alias CasualGame.Game.Game
  alias CasualGame.Game.GamesServer
  alias CasualGame.Users.UsersServer

  describe "General GamesServer tests" do
    setup do
      GamesServer.reset()
      CasualGame.Users.UsersServer.create_user("oleg", "123")
      CasualGame.Users.UsersServer.create_user("roman", "123")

      on_exit(fn ->
        GamesServer.reset()
      end)
    end

    test "can create game" do
      GamesServer.create_challenge("oleg")

      # Match assert to check if we have {:ok, game_id}
      assert {:ok, game_id} = GamesServer.accept_challenge("roman", "oleg")
    end

    test "can make moves using game_id" do
      GamesServer.create_challenge("oleg")

      {:ok, game_id} = GamesServer.accept_challenge("roman", "oleg")
      assert :ok == GamesServer.user_move(game_id, "roman", "rock")
    end

    test "can't move twice" do
      GamesServer.create_challenge("oleg")

      {:ok, game_id} = GamesServer.accept_challenge("roman", "oleg")
      assert :ok == GamesServer.user_move(game_id, "roman", "rock")
      assert {:error, :already_moved} == GamesServer.user_move(game_id, "roman", "paper")
    end

    test "can check game results" do
      GamesServer.create_challenge("oleg")

      {:ok, game_id} = GamesServer.accept_challenge("roman", "oleg")

      pid = GamesServer.game(game_id)
      Game.subscribe(pid, self())

      :ok = GamesServer.user_move(game_id, "roman", "rock")
      :ok = GamesServer.user_move(game_id, "oleg", "paper")
      wait_for_round_results()
      assert %{"oleg" => 1} == GamesServer.game_status(game_id)
    end

    test "can check running games" do
      GamesServer.create_challenge("oleg")
      {:ok, game_id} = GamesServer.accept_challenge("roman", "oleg")

      games = GamesServer.games()
      # Making match assert, to check that game_id is registered
      assert Map.has_key?(games, game_id) == true
    end

    test "games list is cleaned up after game is finished" do
      GamesServer.create_challenge("oleg")

      {:ok, game_id} = GamesServer.accept_challenge("roman", "oleg")
      pid = GamesServer.game(game_id)
      Game.subscribe(pid, self())

      :ok = GamesServer.user_move(game_id, "roman", "rock")
      :ok = GamesServer.user_move(game_id, "oleg", "paper")
      wait_for_round_results()

      :ok = GamesServer.user_move(game_id, "roman", "rock")
      :ok = GamesServer.user_move(game_id, "oleg", "rock")
      wait_for_round_results()
      wait_for_round_results()

      # Still adding a tiny timeout to allow games_server monitor to send it's message
      Process.sleep(200)
      games = GamesServer.games()
      # Making match assert, to check that game_id is registered
      assert Map.has_key?(games, game_id) == false
    end

    test "challenges are cleaned up when game is created" do
      GamesServer.create_challenge("oleg")

      {:ok, _game_id} = GamesServer.accept_challenge("roman", "oleg")
      assert [] == GamesServer.list_challenges()
    end
  end

  describe "Game tests" do
    setup do
      CasualGame.Users.UsersServer.create_user("oleg", "123")
      CasualGame.Users.UsersServer.create_user("roman", "123")

      on_exit(fn ->
        CasualGame.Users.UsersServer.reset()
      end)
    end

    test "It's possible to make a move" do
      {:ok, pid} = Game.start_link(["oleg", "roman"])
      assert :ok == Game.user_move(pid, "oleg", "rock")
      assert :ok == Game.user_move(pid, "roman", "paper")
    end

    test "It's not possible to make a move twice" do
      {:ok, pid} = Game.start_link(["oleg", "roman"])
      assert :ok == Game.user_move(pid, "oleg", "rock")
      assert {:error, :already_moved} == Game.user_move(pid, "oleg", "paper")
    end

    test "Game will finish after given number of rounds" do
      {:ok, pid} = Game.start_link(["oleg", "roman"])
      Game.subscribe(pid, self())

      assert :ok == Game.user_move(pid, "oleg", "rock")
      assert :ok == Game.user_move(pid, "roman", "rock")

      wait_for_round_results()

      assert :ok == Game.user_move(pid, "oleg", "paper")
      assert :ok == Game.user_move(pid, "roman", "rock")

      wait_for_round_results()
      wait_for_round_results()

      Process.sleep(100)
      assert false == Process.alive?(pid)
    end

    test "Game go on if players are idle" do
      Application.put_env(:casual_game, :move_timeout, 400)
      {:ok, pid} = Game.start_link(["oleg", "roman"])
      Game.subscribe(pid, self())
      assert :ok == Game.user_move(pid, "oleg", "rock")

      # wait for results of 2 rounds
      wait_for_round_results()
      wait_for_round_results()

      assert false == Process.alive?(pid)
    end

    test "Leadboards are updated after game is complete" do
      UsersServer.create_user("test1", "123")
      UsersServer.create_user("test2", "123")

      {:ok, pid} = Game.start_link(["test1", "test2"])

      Game.subscribe(pid, self())

      assert :ok == Game.user_move(pid, "test1", "rock")
      assert :ok == Game.user_move(pid, "test2", "rock")

      wait_for_round_results()

      assert :ok == Game.user_move(pid, "test1", "paper")
      assert :ok == Game.user_move(pid, "test2", "rock")

      wait_for_round_results()

      # Wait for final message
      wait_for_round_results()

      assert UsersServer.user_info("test1") == [{"test1", "123", 1, 0}]
      # assert Enum.sort([{"test1", "123", 1, 0}, {"test2", "123", 1, 1}]) ==
      #          Enum.sort(CasualGame.Users.UsersServer.users())
    end
  end

  defp wait_for_round_results() do
    receive do
      _ ->
        :ok
    end
  end
end
