defmodule GamesServerApiTest do
  use ExUnit.Case, async: false
  use Plug.Test

  alias CasualGame.Users.UsersServer
  alias CasualGame.API.GamesServerResource
  alias CasualGame.Game.GamesServer

  describe "Games server resourse test" do
    setup do
      UsersServer.create_user("oleg", "123")
      UsersServer.create_user("roman", "123")

      on_exit(fn ->
        GamesServer.reset()
      end)
    end

    test "can create challenge" do
      conn =
        conn(:post, "/games-server")
        |> assign(:current_user, "oleg")
        |> put_req_header("content-type", "application/json")
        |> GamesServerResource.call([])

      assert conn.status == 200
      resp = conn.resp_body |> Poison.decode!()
      assert resp == %{"ok" => "challenge created"}
    end

    test "can list challenge" do
      GamesServer.create_challenge("roman")

      conn =
        conn(:get, "/games-server")
        |> assign(:current_user, "oleg")
        |> put_req_header("accept", "application/json")
        |> GamesServerResource.call([])

      assert conn.status == 200
      resp = conn.resp_body |> Poison.decode!()
      assert resp == ["roman"]
    end

    test "can't create challenge if already created" do
      GamesServer.create_challenge("oleg")

      conn =
        conn(:post, "/games-server")
        |> assign(:current_user, "oleg")
        |> put_req_header("content-type", "application/json")
        |> GamesServerResource.call([])

      assert conn.status == 409
      resp = conn.resp_body |> Poison.decode!()
      assert resp == %{"error" => "can't create more than one challenge"}
    end

    test "can accept challenge" do
      GamesServer.create_challenge("oleg")

      conn =
        conn(:post, "/games-server")
        |> assign(:current_user, "roman")
        |> Map.put(:body_params, %{"challenge" => "oleg"})
        |> put_req_header("content-type", "application/json")
        |> GamesServerResource.call([])

      assert conn.status == 200
      resp = conn.resp_body |> Poison.decode!()

      # Checking that response matches {"ok" => game_id}
      assert %{"ok" => _game_id} = resp
    end

    test "challenge is removed once accepted" do
      GamesServer.create_challenge("oleg")

      conn =
        conn(:post, "/games-server")
        |> assign(:current_user, "roman")
        |> Map.put(:body_params, %{"challenge" => "oleg"})
        |> put_req_header("content-type", "application/json")
        |> GamesServerResource.call([])

      assert conn.status == 200
      assert GamesServer.list_challenges() == []
    end

    test "can't accept own challenge" do
      GamesServer.create_challenge("oleg")

      conn =
        conn(:post, "/games-server")
        |> assign(:current_user, "oleg")
        |> Map.put(:body_params, %{"challenge" => "oleg"})
        |> put_req_header("content-type", "application/json")
        |> GamesServerResource.call([])

      assert conn.status == 409
      assert GamesServer.list_challenges() == ["oleg"]
    end
  end
end
