defmodule CasualGameTest.UserServerTest do
  use ExUnit.Case, async: false

  alias CasualGame.Users.UsersServer

  describe "General UsersServer tests" do
    setup do
      on_exit(fn ->
        UsersServer.reset()
      end)
    end

    test "can create user" do
      # Creating a user
      assert true == UsersServer.create_user("test", "123")

      # Created user can be fetched from the UsersServer
      assert UsersServer.get_user("test", "123") == {:ok, "test"}
    end

    test "duplicated users are not created" do
      # Creating first user
      UsersServer.create_user("test", "123")

      # Trying to create second user
      assert false == UsersServer.create_user("test", "another pass")

      assert UsersServer.user_info("test") == [{"test", "123", 0, 0}]
    end

    test "impossible to get users with wrong passord" do
      # Creating a user
      UsersServer.create_user("test", "123")

      # Fetching user with wrong credentials
      assert UsersServer.get_user("test", "wrong passowrd") == {:error, :incorrect_credentials}
    end

    test "leadboard can be updated" do
      # Creating a user
      UsersServer.create_user("test", "123")
      UsersServer.update_leadboard("test", false)
      assert UsersServer.users() == [{"test", "123", 1, 0}]
    end
  end
end
