defmodule GameResourceTest do
  use ExUnit.Case, async: false
  use Plug.Test

  alias CasualGame.Users.UsersServer
  alias CasualGame.API.GameResource
  alias CasualGame.Game.GamesServer

  describe "Games api resourse tests" do
    setup do
      UsersServer.create_user("oleg", "123")
      UsersServer.create_user("roman", "123")

      on_exit(fn ->
        GamesServer.reset()
      end)
    end

    test "can't send commands to not existing game" do
      conn =
        conn(:post, "/games/:id", %{"game_id" => 1})
        |> put_req_header("accept", "application/json")
        |> GameResource.call([])

      assert conn.status == 404
    end

    test "can get info about existing game" do
      GamesServer.create_challenge("oleg")
      {:ok, game_id} = GamesServer.accept_challenge("roman", "oleg")

      :ok = GamesServer.user_move(game_id, "roman", "rock")
      :ok = GamesServer.user_move(game_id, "oleg", "paper")
      Process.sleep(100)

      conn =
        conn(:get, "/games/:game_id", %{"game_id" => "#{game_id}"})
        |> put_req_header("accept", "application/json")
        |> GameResource.call([])

      assert conn.status == 200

      resp = conn.resp_body |> Poison.decode!()
      assert resp == %{"oleg" => 1}
    end

    test "can send move to existing game" do
      GamesServer.create_challenge("oleg")
      {:ok, game_id} = GamesServer.accept_challenge("roman", "oleg")

      conn1 =
        conn(:post, "/games/:game_id", %{"game_id" => "#{game_id}"})
        |> assign(:current_user, "oleg")
        |> Map.put(:body_params, %{"game_id" => game_id, "move" => "rock"})
        |> put_req_header("content-type", "application/json")
        |> GameResource.call([])

      assert conn1.status == 200

      conn2 =
        conn(:post, "/games/:game_id", %{"game_id" => "#{game_id}"})
        |> assign(:current_user, "roman")
        |> Map.put(:body_params, %{"game_id" => game_id, "move" => "paper"})
        |> put_req_header("content-type", "application/json")
        |> GameResource.call([])

      assert conn2.status == 200

      conn =
        conn(:get, "/games/:game_id", %{"game_id" => "#{game_id}"})
        |> put_req_header("accept", "application/json")
        |> GameResource.call([])

      assert conn.status == 200

      resp = conn.resp_body |> Poison.decode!()
      assert resp == %{"roman" => 1}
    end
  end
end
