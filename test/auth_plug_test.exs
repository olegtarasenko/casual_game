defmodule AuthPlugTest do
  use ExUnit.Case, async: false
  use Plug.Test

  alias CasualGame.API.AuthPlug
  alias CasualGame.Users.UsersServer

  describe "authentication cases" do
    test "401 response if no authentication token provided" do
      conn =
        conn(:get, "/test", "")
        |> AuthPlug.call([])

      assert conn.status == 401
    end

    test "401 response if authentication is not formatted correctly" do
      conn =
        conn(:get, "/test", "")
        |> put_req_header("authorization", "wrong_username.wrong_pass")
        |> AuthPlug.call([])

      assert conn.status == 401
    end

    test "401 response if authentication is not formatted correctly (case2)" do
      conn =
        conn(:get, "/test", "")
        |> put_req_header("authorization", "wrong_username:wrong_pass")
        |> AuthPlug.call([])

      assert conn.status == 401
    end

    test "authorizes response if user is resolved by a given username/pass" do
      true = UsersServer.create_user("oleg", "123")

      conn =
        conn(:get, "/test", "")
        |> put_req_header("authorization", "oleg:123")
        |> AuthPlug.call([])

      assert conn.assigns() == %{current_user: "oleg"}
    end
  end
end
